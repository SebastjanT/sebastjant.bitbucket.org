
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 
 
function generirajPodatke(stPacienta) {
	var ehrId = "";
    if (stPacienta==1){
		sessionId = getSessionId();
	
	    //genreiraj id/osebo
	    var erhId="";
		var ime = "Popolni";
		var priimek = "Darovalec";
	    var datumRojstva = "1998-05-25T00:00:00.000Z";
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        ehrId = data.ehrId;
			        var partyData = {
			            firstNames: ime,
			            lastNames: priimek,
			            dateOfBirth: datumRojstva,
			            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                    console.log("Uspešno za Pacient 1")
			                    $("#OEHR").append("<option id='1' value='"+ehrId+"'>"+ime +" "+ priimek+"</option>");
								$("#generiranje").append("<p>Popolni Darovalec: "+ehrId+"</p>");
			                }
			            },
			            error: function(err) {
			                console.log("Napaka za Pacient 1");
			            }
			        });
			        
			        //generiraj podatke meritev
		
					//Meritev 1
			        
					var datumInUra = "2018-01-21T10:30Z";
					var telesnaVisina = "185";
					var telesnaTeza = "75.00";
					var sistolicniKrvniTlak = "110";
					var diastolicniKrvniTlak = "69";
					var nasicenostKrviSKisikom = "98";
					var merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						//console.log(parametriZahteve);
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 1");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 1");
						    }
						});
			        
			        //Meritev 2
			        
			         datumInUra = "2018-02-21T10:30Z";
					 telesnaVisina = "185";
					 telesnaTeza = "74.00";
					 sistolicniKrvniTlak = "117";
					 diastolicniKrvniTlak = "70";
					 nasicenostKrviSKisikom = "97";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 2");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 2");
						    }
						});
					
					
					//Meritev 3
					
					
					 datumInUra = "2018-03-21T10:30Z";
					 telesnaVisina = "185";
					 telesnaTeza = "76.00";
					 sistolicniKrvniTlak = "109";
					 diastolicniKrvniTlak = "65";
					 nasicenostKrviSKisikom = "99";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 3");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 3");
						    }
						});
					
					
					//Meritev 4
					
					
					 datumInUra = "2018-04-21T10:30Z";
					 telesnaVisina = "185";
					 telesnaTeza = "77.00";
					 sistolicniKrvniTlak = "119";
					 diastolicniKrvniTlak = "79";
					 nasicenostKrviSKisikom = "98";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 4");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 4");
						    }
						});
					
					
					//Meritev 5
					
					
					 datumInUra = "2018-05-21T10:30Z";
					 telesnaVisina = "185";
					 telesnaTeza = "74.00";
					 sistolicniKrvniTlak = "111";
					 diastolicniKrvniTlak = "71";
					 nasicenostKrviSKisikom = "98";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 5");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 5");
						    }
						});
			        
			    }
			});

    }
    else if(stPacienta==2){
    	sessionId = getSessionId();
	
	    //genreiraj id/osebo
	    var erhId="";
		var ime = "Možnji";
		var priimek = "Darovalec";
	    var datumRojstva = "1998-05-28T00:00:00.000Z";
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        ehrId = data.ehrId;
			        var partyData = {
			            firstNames: ime,
			            lastNames: priimek,
			            dateOfBirth: datumRojstva,
			            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                    console.log("Uspešno za Pacient 2")
			                    $("#OEHR").append("<option id='2' value='"+ehrId+"'>"+ime +" "+ priimek+"</option>");
								$("#generiranje").append("<p>Možni Darovalec: "+ehrId+"</p>");
			                }
			            },
			            error: function(err) {
			                console.log("Napaka za Pacient 2");
			            }
			        });
			        
			        //generiraj podatke meritev
		
					//Meritev 1
			        
					var datumInUra = "2018-01-21T10:30Z";
					var telesnaVisina = "160";
					var telesnaTeza = "55.00";
					var sistolicniKrvniTlak = "120";
					var diastolicniKrvniTlak = "69";
					var nasicenostKrviSKisikom = "98";
					var merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						//console.log(parametriZahteve);
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 1");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 1");
						    }
						});
			        
			        //Meritev 2
			        
			         datumInUra = "2018-02-21T10:30Z";
					 telesnaVisina = "161";
					 telesnaTeza = "56.00";
					 sistolicniKrvniTlak = "124";
					 diastolicniKrvniTlak = "80";
					 nasicenostKrviSKisikom = "97";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 2");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 2");
						    }
						});
					
					
					//Meritev 3
					
					
					 datumInUra = "2018-03-21T10:30Z";
					 telesnaVisina = "160";
					 telesnaTeza = "54.00";
					 sistolicniKrvniTlak = "130";
					 diastolicniKrvniTlak = "81";
					 nasicenostKrviSKisikom = "99";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 3");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 3");
						    }
						});
					
					
					//Meritev 4
					
					
					 datumInUra = "2018-04-21T10:30Z";
					 telesnaVisina = "161";
					 telesnaTeza = "55.00";
					 sistolicniKrvniTlak = "119";
					 diastolicniKrvniTlak = "79";
					 nasicenostKrviSKisikom = "98";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 4");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 4");
						    }
						});
					
					
					//Meritev 5
					
					
					 datumInUra = "2018-05-21T10:30Z";
					 telesnaVisina = "160";
					 telesnaTeza = "56.00";
					 sistolicniKrvniTlak = "120";
					 diastolicniKrvniTlak = "71";
					 nasicenostKrviSKisikom = "98";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 5");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 5");
						    }
						});
			        
			    }
			});
    	
    	
    }
    else if(stPacienta==3){
    	sessionId = getSessionId();
	
	    //genreiraj id/osebo
	    var erhId="";
		var ime = "Neustrezni";
		var priimek = "Darovalec";
	    var datumRojstva = "1998-05-28T00:00:00.000Z";
			$.ajaxSetup({
			    headers: {"Ehr-Session": sessionId}
			});
			$.ajax({
			    url: baseUrl + "/ehr",
			    type: 'POST',
			    success: function (data) {
			        ehrId = data.ehrId;
			        var partyData = {
			            firstNames: ime,
			            lastNames: priimek,
			            dateOfBirth: datumRojstva,
			            partyAdditionalInfo: [{key: "ehrId", value: ehrId}]
			        };
			        $.ajax({
			            url: baseUrl + "/demographics/party",
			            type: 'POST',
			            contentType: 'application/json',
			            data: JSON.stringify(partyData),
			            success: function (party) {
			                if (party.action == 'CREATE') {
			                    console.log("Uspešno za Pacient 3")
			                    $("#OEHR").append("<option id='3' value='"+ehrId+"'>"+ime +" "+ priimek+"</option>");
								$("#generiranje").append("<p>Neustrezni Darovalec: "+ehrId+"</p>");
								$("#generiranje").append("<p>Do osvežitve strani so novo generirani ehrID-ji na voljo todi v spustnem seznamu izbire pacienta.</p>");
			                }
			            },
			            error: function(err) {
			                console.log("Napaka za Pacient 3");
			            }
			        });
			        
			        //generiraj podatke meritev
		
					//Meritev 1
			        
					var datumInUra = "2018-01-21T10:30Z";
					var telesnaVisina = "175";
					var telesnaTeza = "90.00";
					var sistolicniKrvniTlak = "130";
					var diastolicniKrvniTlak = "89";
					var nasicenostKrviSKisikom = "98";
					var merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						//console.log(parametriZahteve);
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 1");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 1");
						    }
						});
			        
			        //Meritev 2
			        
			         datumInUra = "2018-02-21T10:30Z";
					 telesnaVisina = "177";
					 telesnaTeza = "91.00";
					 sistolicniKrvniTlak = "145";
					 diastolicniKrvniTlak = "100";
					 nasicenostKrviSKisikom = "97";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 2");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 2");
						    }
						});
					
					
					//Meritev 3
					
					
					 datumInUra = "2018-03-21T10:30Z";
					 telesnaVisina = "176";
					 telesnaTeza = "93.00";
					 sistolicniKrvniTlak = "150";
					 diastolicniKrvniTlak = "95";
					 nasicenostKrviSKisikom = "99";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 3");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 3");
						    }
						});
					
					
					//Meritev 4
					
					
					 datumInUra = "2018-04-21T10:30Z";
					 telesnaVisina = "175";
					 telesnaTeza = "92.00";
					 sistolicniKrvniTlak = "135";
					 diastolicniKrvniTlak = "89";
					 nasicenostKrviSKisikom = "98";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 4");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 4");
						    }
						});
					
					
					//Meritev 5
					
					
					 datumInUra = "2018-05-21T10:30Z";
					 telesnaVisina = "175";
					 telesnaTeza = "90.00";
					 sistolicniKrvniTlak = "138";
					 diastolicniKrvniTlak = "85";
					 nasicenostKrviSKisikom = "98";
					 merilec = "Sestra Sestra";
				
				
						$.ajaxSetup({
						    headers: {"Ehr-Session": sessionId}
						});
						var podatki = {
						    "ctx/language": "en",
						    "ctx/territory": "SI",
						    "ctx/time": datumInUra,
						    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
						    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
						    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
						    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
						    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
						    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
						};
						var parametriZahteve = {
						    ehrId: ehrId,
						    templateId: 'Vital Signs',
						    format: 'FLAT',
						    committer: merilec
						};
						$.ajax({
						    url: baseUrl + "/composition?" + $.param(parametriZahteve),
						    type: 'POST',
						    contentType: 'application/json',
						    data: JSON.stringify(podatki),
						    success: function (res) {
						        console.log("Uspešno dodana meritev 5");
						    },
						    error: function(err) {
						    	console.log("Napaka meritev 5");
						    }
						});
			        
			    }
			});
    	
    	
    	
    }
  


  // TODO: Potrebno implementirati
  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija

function izpolniOS() {
	sessionId = getSessionId();

	var ehrId = $("#pEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#ime").html("");
		$("#priimek").html("");
		$("#rd").html("");
		$("#visina").html("");
		$("#teza").html("");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				//console.log(party);
				$("#ime").html(""+party.firstNames+"");
				$("#priimek").html(""+party.lastNames+"");
				$("#rd").html(""+party.dateOfBirth+"");
				$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "height",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	//console.log(res);
					    	if (res.length > 0) {
						    	$("#visina").html(""+res[0].height+" cm");
					    	}
					    },
					    error: function() {
					    	
					    }
			}),
			$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "weight",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	//console.log(res);
					    	if (res.length > 0) {
						    	$("#teza").html(""+res[0].weight+" kg");
					    	}
					    },
					    error: function() {
					    	
					    }
			});
			},
			error: function(err) {
				
			}
		});
	}
}


	function izpisMeritev() {
	sessionId = getSessionId();

	var ehrId = $("#pEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#izpisPritS").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
				$("#izpisPrit").html("<br/><span>Pridobivanje " +
          "podatkov za pacienta <b>'" + party.firstNames +
          " " + party.lastNames + "'</b>.</span><br/><br/>");
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	//console.log(res);
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th>Diastolični krvni tlak</th><th>Sistolični krvni tlak</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td>" + res[i].systolic +
                          " " + res[i].unit + "</td><td>" + res[i].diastolic +
                          " " + res[i].unit + "</td></tr>";
						        }
						        results += "</table>";
						        $("#izpisPrit").append(results);
					    	} else {
					    		$("#izpisPritS").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#izpisPritS").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "spO2",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	//console.log(res);
					    	if (res.length > 0) {
						    	var results = "<table class='table table-striped " +
                    "table-hover'><tr><th>Datum in ura</th>" +
                    "<th class='text-right'>Nasičenost krvi s kisikom</th></tr>";
						        for (var i in res) {
						            results += "<tr><td>" + res[i].time +
                          "</td><td class='text-right'>" + res[i].spO2 +
                          " %</td></tr>";
						        }
						        results += "</table>";
						        $("#izpisPrit").append(results);
					    	} else {
					    		$("#izpisPritS").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#izpisPritS").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
	    	},
	    	error: function(err) {
	    		$("#izpisPritS").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}

function preveriStanje() {
	$("#izpisPrit2").html("");
	sessionId = getSessionId();

	var ehrId = $("#pEHRid").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#izpisPritS2").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
	    	type: 'GET',
	    	headers: {"Ehr-Session": sessionId},
	    	success: function (data) {
				var party = data.party;
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	//console.log(res);
					    	if (res.length > 0) {
						    	var stanje=[];
						    	var povpS=0;
						    	var povpD=0;
						        for (var i in res){
						        	if (res[i].systolic<120 && res[i].diastolic<80){
						        		stanje[i]=0;
						        	}
						        	else if(res[i].systolic<140 && res[i].diastolic<89){
						        		stanje[i]=1;
						        	}
						        	else{
						        		stanje[i]=2;
						        	}
						        	povpS+=res[i].systolic;
						        	povpD+=res[i].diastolic;
						        }
						        povpS/=stanje.length;
						        povpD/=stanje.length;
						        //console.log(stanje);
						        var st0=0;
						        var st1=0;
						        var st2=0;
						        for (var i in stanje){
						        	if(stanje[i]==0){
						        		st0++;
						        	}
						        	else if(stanje[i]==1){
						        		st1++;
						        	}
						        	else{
						        		st2++;
						        	}
						        }
						        //console.log(st0+" "+st1+" "+st2);
						        if (st0>st1 && st0>st2){
						        	$("#izpisPrit2").append("<div class='col-lg-16 col-md-16 col-sm-16'><strong>Vaša povprečna ravn krvnega tlaku je popolna!</strong></br></br></div><svg id='fillgauge4' width='19%' height='200'></svg><svg id='fillgauge5' width='19%' height='200'></svg>\
						<script language='JavaScript'>\
						var config3 = liquidFillGaugeDefaultSettings();\
						config3.circleColor = 'green';\
			config3.textColor = 'black';\
    		config3.waveTextColor = 'black';\
    		config3.waveColor = 'green';\
            config3.textVertPosition = 0.8;\
            config3.waveAnimateTime = 5000;\
            config3.waveHeight = 0.15;\
            config3.waveAnimate = false;\
            config3.waveOffset = 0.25;\
            config3.valueCountUp = false;\
            config3.displayPercent = false;\
						var gauge4 = loadLiquidFillGauge('fillgauge4', "+povpS+", config3);\
						var gauge5 = loadLiquidFillGauge('fillgauge5', "+povpD+", config3);\
						</script><br/>\
						<iframe width='560' height='315' src='https://www.youtube.com/embed/DNyKDI9pn0Q?rel=0&amp;controls=0&amp;showinfo=0' frameborder='0' allow='autoplay; encrypted-media' allowfullscreen></iframe>");
						        }
						        else if(st1>st0 && st1>st2){
						        	$("#izpisPrit2").append("<div class='col-lg-16 col-md-16 col-sm-16'><strong>Vaša povprečna ravn krvnega tlaku je sprejemljiva!</strong></br></br></div><svg id='fillgauge4' width='19%' height='200'></svg><svg id='fillgauge5' width='19%' height='200'></svg>\
						<script language='JavaScript'>\
						var config3 = liquidFillGaugeDefaultSettings();\
						config3.circleColor = 'orange';\
			config3.textColor = 'black';\
    		config3.waveTextColor = 'black';\
    		config3.waveColor = 'orange';\
            config3.textVertPosition = 0.8;\
            config3.waveAnimateTime = 5000;\
            config3.waveHeight = 0.15;\
            config3.waveAnimate = false;\
            config3.waveOffset = 0.25;\
            config3.valueCountUp = false;\
            config3.displayPercent = false;\
						var gauge4 = loadLiquidFillGauge('fillgauge4', "+povpS+", config3);\
						var gauge5 = loadLiquidFillGauge('fillgauge5', "+povpD+", config3);\
						</script>");
						        }
						        else{
						        	$("#izpisPrit2").append("<div class='col-lg-16 col-md-16 col-sm-16'><strong>Vaša povprečna ravn krvnega tlaku je nesprejemljiva!</strong></br></br></div><svg id='fillgauge4' width='19%' height='200'></svg><svg id='fillgauge5' width='19%' height='200'></svg>\
						<script language='JavaScript'>\
						var config3 = liquidFillGaugeDefaultSettings();\
						config3.circleColor = 'red';\
			config3.textColor = 'black';\
    		config3.waveTextColor = 'black';\
    		config3.waveColor = 'red';\
            config3.textVertPosition = 0.8;\
            config3.waveAnimateTime = 5000;\
            config3.waveHeight = 0.15;\
            config3.waveAnimate = false;\
            config3.waveOffset = 0.25;\
            config3.valueCountUp = false;\
            config3.displayPercent = false;\
						var gauge4 = loadLiquidFillGauge('fillgauge4', "+povpS+", config3);\
						var gauge5 = loadLiquidFillGauge('fillgauge5', "+povpD+", config3);\
						</script>");
						        }
					    	} else {
					    		$("#izpisPritS").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#izpisPritS2").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
					$.ajax({
  					    url: baseUrl + "/view/" + ehrId + "/" + "spO2",
					    type: 'GET',
					    headers: {"Ehr-Session": sessionId},
					    success: function (res) {
					    	//console.log(res);
					    	if (res.length > 0) {
						    	var stanje=[];
						        for (var i in res){
						        	if (res[i].spO2>90){
						        		stanje[i]=0;
						        	}
						        	else{
						        		stanje[i]=1;
						        	}
						        }
						        //console.log(stanje);
						        var st0=0;
						        var st1=0;
						        for (var i in stanje){
						        	if(stanje[i]==0){
						        		st0++;
						        	}
						        	else if(stanje[i]==1){
						        		st1++;
						        	}
						        }
						        //console.log(st0+" "+st1+" "+st2);
						        if (st0>st1){
						        	$("#izpisPrit2").append("<div class='col-lg-16 col-md-16 col-sm-16'><strong>Vaša ravn kisika v krvi je popolna!</strong></div>");
						        }
						        else{
						        	$("#izpisPrit2").append("<div class='col-lg-16 col-md-16 col-sm-16'><strong>Vaša ravn kisika v krvi je nesprejemljiva!</strong></div>");
						        }
					    	} else {
					    		$("#izpisPritS2").html(
                    "<span class='obvestilo label label-warning fade-in'>" +
                    "Ni podatkov!</span>");
					    	}
					    },
					    error: function() {
					    	$("#izpisPritS2").html(
                  "<span class='obvestilo label label-danger fade-in'>Napaka '" +
                  JSON.parse(err.responseText).userMessage + "'!");
					    }
					});
	    	},
	    	error: function(err) {
	    		$("#izpisPritS2").html(
            "<span class='obvestilo label label-danger fade-in'>Napaka '" +
            JSON.parse(err.responseText).userMessage + "'!");
	    	}
		});
	}
}




$(document).ready(function() {
	
	$('#OEHR').change(function() {
		$("#pEHRid").val($(this).val());
		izpolniOS();
	});
	
	/*$('#1').html(function() {
		console.log($("#1").val());
	});*/
	
	$("a#Generate").click(function(){
		$("#OEHR").html("<option value=''></option>");
		var ehrId1=generirajPodatke(1);
		
		var ehrId2=generirajPodatke(2);
		
		var ehrId3=generirajPodatke(3);
		
		
	});
	

	
});



function liquidFillGaugeDefaultSettings(){
    return {
        minValue: 0, // The gauge minimum value.
        maxValue: 100, // The gauge maximum value.
        circleThickness: 0.05, // The outer circle thickness as a percentage of it's radius.
        circleFillGap: 0.05, // The size of the gap between the outer circle and wave circle as a percentage of the outer circles radius.
        circleColor: "#178BCA", // The color of the outer circle.
        waveHeight: 0.05, // The wave height as a percentage of the radius of the wave circle.
        waveCount: 1, // The number of full waves per width of the wave circle.
        waveRiseTime: 1000, // The amount of time in milliseconds for the wave to rise from 0 to it's final height.
        waveAnimateTime: 18000, // The amount of time in milliseconds for a full wave to enter the wave circle.
        waveRise: true, // Control if the wave should rise from 0 to it's full height, or start at it's full height.
        waveHeightScaling: true, // Controls wave size scaling at low and high fill percentages. When true, wave height reaches it's maximum at 50% fill, and minimum at 0% and 100% fill. This helps to prevent the wave from making the wave circle from appear totally full or empty when near it's minimum or maximum fill.
        waveAnimate: true, // Controls if the wave scrolls or is static.
        waveColor: "#178BCA", // The color of the fill wave.
        waveOffset: 0, // The amount to initially offset the wave. 0 = no offset. 1 = offset of one full wave.
        textVertPosition: .5, // The height at which to display the percentage text withing the wave circle. 0 = bottom, 1 = top.
        textSize: 1, // The relative height of the text to display in the wave circle. 1 = 50%
        valueCountUp: true, // If true, the displayed value counts up from 0 to it's final value upon loading. If false, the final value is displayed.
        displayPercent: true, // If true, a % symbol is displayed after the value.
        textColor: "#045681", // The color of the value text when the wave does not overlap it.
        waveTextColor: "#A4DBf8" // The color of the value text when the wave overlaps it.
    };
}


function loadLiquidFillGauge(elementId, value, config) {
    if(config == null) config = liquidFillGaugeDefaultSettings();

    var gauge = d3.select("#" + elementId);
    var radius = Math.min(parseInt(gauge.style("width")), parseInt(gauge.style("height")))/2;
    var locationX = parseInt(gauge.style("width"))/2 - radius;
    var locationY = parseInt(gauge.style("height"))/2 - radius;
    var fillPercent = Math.max(config.minValue, Math.min(config.maxValue, value))/config.maxValue;

    var waveHeightScale;
    if(config.waveHeightScaling){
        waveHeightScale = d3.scale.linear()
            .range([0,config.waveHeight,0])
            .domain([0,10,35]);
    } else {
        waveHeightScale = d3.scale.linear()
            .range([config.waveHeight,config.waveHeight])
            .domain([0,100]);
    }

    var textPixels = (config.textSize*radius/2);
    var textFinalValue = parseFloat(value).toFixed(2);
    var textStartValue = config.valueCountUp?config.minValue:textFinalValue;
    var percentText = config.displayPercent?"%":"";
    var circleThickness = config.circleThickness * radius;
    var circleFillGap = config.circleFillGap * radius;
    var fillCircleMargin = circleThickness + circleFillGap;
    var fillCircleRadius = radius - fillCircleMargin;
    var waveHeight = fillCircleRadius*waveHeightScale(fillPercent*100);

    var waveLength = fillCircleRadius*2/config.waveCount;
    var waveClipCount = 1+config.waveCount;
    var waveClipWidth = waveLength*waveClipCount;

    // Rounding functions so that the correct number of decimal places is always displayed as the value counts up.
    var textRounder = function(value){ return Math.round(value); };
    if(parseFloat(textFinalValue) != parseFloat(textRounder(textFinalValue))){
        textRounder = function(value){ return parseFloat(value).toFixed(1); };
    }
    if(parseFloat(textFinalValue) != parseFloat(textRounder(textFinalValue))){
        textRounder = function(value){ return parseFloat(value).toFixed(2); };
    }

    // Data for building the clip wave area.
    var data = [];
    for(var i = 0; i <= 40*waveClipCount; i++){
        data.push({x: i/(40*waveClipCount), y: (i/(40))});
    }

    // Scales for drawing the outer circle.
    var gaugeCircleX = d3.scale.linear().range([0,2*Math.PI]).domain([0,1]);
    var gaugeCircleY = d3.scale.linear().range([0,radius]).domain([0,radius]);

    // Scales for controlling the size of the clipping path.
    var waveScaleX = d3.scale.linear().range([0,waveClipWidth]).domain([0,1]);
    var waveScaleY = d3.scale.linear().range([0,waveHeight]).domain([0,1]);

    // Scales for controlling the position of the clipping path.
    var waveRiseScale = d3.scale.linear()
        // The clipping area size is the height of the fill circle + the wave height, so we position the clip wave
        // such that the it will overlap the fill circle at all when at 0%, and will totally cover the fill
        // circle at 100%.
        .range([(fillCircleMargin+fillCircleRadius*2+waveHeight),(fillCircleMargin-waveHeight)])
        .domain([0,1]);
    var waveAnimateScale = d3.scale.linear()
        .range([0, waveClipWidth-fillCircleRadius*2]) // Push the clip area one full wave then snap back.
        .domain([0,1]);

    // Scale for controlling the position of the text within the gauge.
    var textRiseScaleY = d3.scale.linear()
        .range([fillCircleMargin+fillCircleRadius*2,(fillCircleMargin+textPixels*0.7)])
        .domain([0,1]);

    // Center the gauge within the parent SVG.
    var gaugeGroup = gauge.append("g")
        .attr('transform','translate('+locationX+','+locationY+')');

    // Draw the outer circle.
    var gaugeCircleArc = d3.svg.arc()
        .startAngle(gaugeCircleX(0))
        .endAngle(gaugeCircleX(1))
        .outerRadius(gaugeCircleY(radius))
        .innerRadius(gaugeCircleY(radius-circleThickness));
    gaugeGroup.append("path")
        .attr("d", gaugeCircleArc)
        .style("fill", config.circleColor)
        .attr('transform','translate('+radius+','+radius+')');

    // Text where the wave does not overlap.
    var text1 = gaugeGroup.append("text")
        .text(textRounder(textStartValue) + percentText)
        .attr("class", "liquidFillGaugeText")
        .attr("text-anchor", "middle")
        .attr("font-size", textPixels + "px")
        .style("fill", config.textColor)
        .attr('transform','translate('+radius+','+textRiseScaleY(config.textVertPosition)+')');

    // The clipping wave area.
    var clipArea = d3.svg.area()
        .x(function(d) { return waveScaleX(d.x); } )
        .y0(function(d) { return waveScaleY(Math.sin(Math.PI*2*config.waveOffset*-1 + Math.PI*2*(1-config.waveCount) + d.y*2*Math.PI));} )
        .y1(function(d) { return (fillCircleRadius*2 + waveHeight); } );
    var waveGroup = gaugeGroup.append("defs")
        .append("clipPath")
        .attr("id", "clipWave" + elementId);
    var wave = waveGroup.append("path")
        .datum(data)
        .attr("d", clipArea)
        .attr("T", 0);

    // The inner circle with the clipping wave attached.
    var fillCircleGroup = gaugeGroup.append("g")
        .attr("clip-path", "url(#clipWave" + elementId + ")");
    fillCircleGroup.append("circle")
        .attr("cx", radius)
        .attr("cy", radius)
        .attr("r", fillCircleRadius)
        .style("fill", config.waveColor);

    // Text where the wave does overlap.
    var text2 = fillCircleGroup.append("text")
        .text(textRounder(textStartValue) + percentText)
        .attr("class", "liquidFillGaugeText")
        .attr("text-anchor", "middle")
        .attr("font-size", textPixels + "px")
        .style("fill", config.waveTextColor)
        .attr('transform','translate('+radius+','+textRiseScaleY(config.textVertPosition)+')');

    // Make the value count up.
    if(config.valueCountUp){
        var textTween = function(){
            var i = d3.interpolate(this.textContent, textFinalValue);
            return function(t) { this.textContent = textRounder(i(t)) + percentText; }
        };
        text1.transition()
            .duration(config.waveRiseTime)
            .tween("text", textTween);
        text2.transition()
            .duration(config.waveRiseTime)
            .tween("text", textTween);
    }

    // Make the wave rise. wave and waveGroup are separate so that horizontal and vertical movement can be controlled independently.
    var waveGroupXPosition = fillCircleMargin+fillCircleRadius*2-waveClipWidth;
    if(config.waveRise){
        waveGroup.attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(0)+')')
            .transition()
            .duration(config.waveRiseTime)
            .attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(fillPercent)+')')
            .each("start", function(){ wave.attr('transform','translate(1,0)'); }); // This transform is necessary to get the clip wave positioned correctly when waveRise=true and waveAnimate=false. The wave will not position correctly without this, but it's not clear why this is actually necessary.
    } else {
        waveGroup.attr('transform','translate('+waveGroupXPosition+','+waveRiseScale(fillPercent)+')');
    }

    if(config.waveAnimate) animateWave();

    function animateWave() {
        wave.attr('transform','translate('+waveAnimateScale(wave.attr('T'))+',0)');
        wave.transition()
            .duration(config.waveAnimateTime * (1-wave.attr('T')))
            .ease('linear')
            .attr('transform','translate('+waveAnimateScale(1)+',0)')
            .attr('T', 1)
            .each('end', function(){
                wave.attr('T', 0);
                animateWave(config.waveAnimateTime);
            });
    }

    function GaugeUpdater(){
        this.update = function(value){
            var newFinalValue = parseFloat(value).toFixed(2);
            var textRounderUpdater = function(value){ return Math.round(value); };
            if(parseFloat(newFinalValue) != parseFloat(textRounderUpdater(newFinalValue))){
                textRounderUpdater = function(value){ return parseFloat(value).toFixed(1); };
            }
            if(parseFloat(newFinalValue) != parseFloat(textRounderUpdater(newFinalValue))){
                textRounderUpdater = function(value){ return parseFloat(value).toFixed(2); };
            }

            var textTween = function(){
                var i = d3.interpolate(this.textContent, parseFloat(value).toFixed(2));
                return function(t) { this.textContent = textRounderUpdater(i(t)) + percentText; }
            };

            text1.transition()
                .duration(config.waveRiseTime)
                .tween("text", textTween);
            text2.transition()
                .duration(config.waveRiseTime)
                .tween("text", textTween);

            var fillPercent = Math.max(config.minValue, Math.min(config.maxValue, value))/config.maxValue;
            var waveHeight = fillCircleRadius*waveHeightScale(fillPercent*100);
            var waveRiseScale = d3.scale.linear()
                // The clipping area size is the height of the fill circle + the wave height, so we position the clip wave
                // such that the it will overlap the fill circle at all when at 0%, and will totally cover the fill
                // circle at 100%.
                .range([(fillCircleMargin+fillCircleRadius*2+waveHeight),(fillCircleMargin-waveHeight)])
                .domain([0,1]);
            var newHeight = waveRiseScale(fillPercent);
            var waveScaleX = d3.scale.linear().range([0,waveClipWidth]).domain([0,1]);
            var waveScaleY = d3.scale.linear().range([0,waveHeight]).domain([0,1]);
            var newClipArea;
            if(config.waveHeightScaling){
                newClipArea = d3.svg.area()
                    .x(function(d) { return waveScaleX(d.x); } )
                    .y0(function(d) { return waveScaleY(Math.sin(Math.PI*2*config.waveOffset*-1 + Math.PI*2*(1-config.waveCount) + d.y*2*Math.PI));} )
                    .y1(function(d) { return (fillCircleRadius*2 + waveHeight); } );
            } else {
                newClipArea = clipArea;
            }

            var newWavePosition = config.waveAnimate?waveAnimateScale(1):0;
            wave.transition()
                .duration(0)
                .transition()
                .duration(config.waveAnimate?(config.waveAnimateTime * (1-wave.attr('T'))):(config.waveRiseTime))
                .ease('linear')
                .attr('d', newClipArea)
                .attr('transform','translate('+newWavePosition+',0)')
                .attr('T','1')
                .each("end", function(){
                    if(config.waveAnimate){
                        wave.attr('transform','translate('+waveAnimateScale(0)+',0)');
                        animateWave(config.waveAnimateTime);
                    }
                });
            waveGroup.transition()
                .duration(config.waveRiseTime)
                .attr('transform','translate('+waveGroupXPosition+','+newHeight+')')
        }
    }

    return new GaugeUpdater();
}